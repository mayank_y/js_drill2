const problem1 = (data) => {
  //    Given the dataset of individuals, write a function that accesses and returns the email addresses of all individuals.
  if (
    typeof data == "string" ||
    typeof data == "boolean" ||
    typeof data == "number" ||
    typeof data == "symbol" ||
    data == null
  ) {
    data == null
      ? console.log("Input is null, send a valid input.")
      : console.log(`Input is ${typeof data}, send a valid input`);
    return;
  } else if (data.length == 0) {
    console.log(`Empty data, please send a valid data`);
    return;
  }

  let email_names = [];
  for (let num in data) {
    let obe = data[num];
    email_names.push(obe.email);
  }
  return email_names;
};

module.exports = problem1;
