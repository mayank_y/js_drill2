const problem3 = (data) => {
  let user_data = [];
  //    Create a function that extracts and displays the names of individuals who are students (`isStudent: true`) and live in Australia.

  if (
    typeof data == "string" ||
    typeof data == "boolean" ||
    typeof data == "number" ||
    typeof data == "symbol" ||
    data == null
  ) {
    data == null
      ? console.log("Input is null, send a valid input.")
      : console.log(`Input is ${typeof data}, send a valid input`);
    return;
  } else if (data.length == 0) {
    console.log(`Empty data, please send a valid data`);
    return;
  }
  for (let num in data) {
    let obe = data[num];

    if (obe.isStudent == true && obe.country == "Australia") {
      console.log(`${obe.name} is a student in Australia`);
      user_data.push(obe.name);
    }
  }
  user_data.length == 0
    ? console.log("No student are there in Australia")
    : true;
  return user_data;
};

module.exports = problem3;
