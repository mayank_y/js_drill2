const problem8 = (data) => {
  //    Implement a loop to access and log the city and country of each individual in the dataset.
  if (
    typeof data == "string" ||
    typeof data == "boolean" ||
    typeof data == "number" ||
    typeof data == "symbol" ||
    data == null
  ) {
    data == null
      ? console.log("Input is null, send a valid input.")
      : console.log(`Input is ${typeof data}, send a valid input`);
    return;
  } else if (data.length == 0) {
    console.log(`Empty data, please send a valid data`);
    return;
  }

  for (let num in data) {
    let obe = data[num];

    console.log(`${obe.name} lives in ${obe.city}, ${obe.country}`);
  }
};

module.exports = problem8;
