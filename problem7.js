const problem7 = (data) => {
  //    Write a function that accesses and prints the names and email addresses of individuals aged 25.
  if (
    typeof data == "string" ||
    typeof data == "boolean" ||
    typeof data == "number" ||
    typeof data == "symbol" ||
    data == null
  ) {
    data == null
      ? console.log("Input is null, send a valid input.")
      : console.log(`Input is ${typeof data}, send a valid input`);
    return;
  } else if (data.length == 0) {
    console.log(`Empty data, please send a valid data`);
    return;
  }

  console.log("Name and email of users with age 25 :");
  for (let num in data) {
    if (data[num].age == 25)
      console.log(`${data[num].name} : ${data[num].email}`);
  }
};

module.exports = problem7;
