const problem6 = (data) => {
  //    Create a function to retrieve and display the first hobby of each individual in the dataset.
  if (
    typeof data == "string" ||
    typeof data == "boolean" ||
    typeof data == "number" ||
    typeof data == "symbol" ||
    data == null
  ) {
    data == null
      ? console.log("Input is null, send a valid input.")
      : console.log(`Input is ${typeof data}, send a valid input`);
    return;
  } else if (data.length == 0) {
    console.log(`Empty data, please send a valid data`);
    return;
  }

  for (let num in data) {
    console.log(`${data[num].name}'s first hobby is ${data[num].hobbies[0]}`);
  }
};

module.exports = problem6;
