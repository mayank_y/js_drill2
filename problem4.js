const problem4 = (data) => {
  //    Write a function that accesses and logs the name and city of the individual at the index position 3 in the dataset.
  if (
    typeof data == "string" ||
    typeof data == "boolean" ||
    typeof data == "number" ||
    typeof data == "symbol" ||
    data == null
  ) {
    data == null
      ? console.log("Input is null, send a valid input.")
      : console.log(`Input is ${typeof data}, send a valid input`);
    return;
  } else if (data.length == 0) {
    console.log(`Empty data, please send a valid data`);
    return;
  }

  for (let num in data) {
    if (num == 3) {
      console.log(
        `user at index 3 is ${data[num].name} and lives in ${data[num].city}`
      );
    }
  }
};

module.exports = problem4;
