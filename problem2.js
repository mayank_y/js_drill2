const problem2 = (data) => {
  //    Implement a function that retrieves and prints the hobbies of individuals with a specific age, say 30 years old.
  if (
    typeof data == "string" ||
    typeof data == "boolean" ||
    typeof data == "number" ||
    typeof data == "symbol" ||
    data == null
  ) {
    data == null
      ? console.log("Input is null, send a valid input.")
      : console.log(`Input is ${typeof data}, send a valid input`);
    return;
  } else if (data.length == 0) {
    console.log(`Empty data, please send a valid data`);
    return;
  }
  let user_data = [];

  for (let num in data) {
    let obe = data[num];
    if (obe.age == 30) {
      console.log(
        `${obe.name} with age ${obe.age} has hobbies : ${obe.hobbies}`
      );
      let user_obj = {
        name: obe.name,
        age: obe.age,
        hobbies: obe.hobbies,
      };
      //pushing an array because alot of users might be of age 30
      user_data.push(user_obj);
    }
  }
  return user_data;
};

module.exports = problem2;
