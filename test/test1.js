const data = require("../data");
const problem1 = require("../problem1");
//    Given the dataset of individuals, write a function that accesses and returns the email addresses of all individuals.

const test1 = (data) => {
  return problem1(data);
};
let emails = test1(data);
console.log(`email of all the people are : ${emails}`);
