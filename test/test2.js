const data = require("../data");
const problem2 = require("../problem2");
//    Implement a function that retrieves and prints the hobbies of individuals with a specific age, say 30 years old.

const test2 = (data) => {
  return problem2(data);
};
let user_data = test2(data);
console.log(user_data);
