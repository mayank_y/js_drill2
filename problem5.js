const problem5 = (data) => {
  //    Implement a loop to access and print the ages of all individuals in the dataset.
  if (
    typeof data == "string" ||
    typeof data == "boolean" ||
    typeof data == "number" ||
    typeof data == "symbol" ||
    data == null
  ) {
    data == null
      ? console.log("Input is null, send a valid input.")
      : console.log(`Input is ${typeof data}, send a valid input`);
    return;
  } else if (data.length == 0) {
    console.log(`Empty data, please send a valid data`);
    return;
  }

  for (let num in data) {
    console.log(`${data[num].name} is ${data[num].age} years old`);
  }
};

module.exports = problem5;
